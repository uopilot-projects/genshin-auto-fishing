Автоматизированный процесс рыбалки
==================================

Данный скрипт автоматизирует процесс рыбалки в игре Genshin Impact.

Скрипт рассчитан на разрешение экрана и игры 1920×1080.

И небольшой пример работы программы

[Видео на YouTube](https://www.youtube.com/watch?v=GokH8F8A7iM)

Ниже представлен код скрипта. Для его работы необходимо указать путь к двум файлам-картинкам target.png и cursor.png в строках 27 и 28, скачать которые можно по ссылке внизу страницы.

    // Genshin impact рыбалка 2.0
    set #bar_x1 710
    set #bar_y1 95
    set #bar_x2 1205
    set #bar_y2 135
    set #golden_frame_color 12648447
    set #cursor_color 12648447
    set #n_golden_frame 0
    set #cur_sdvig - 40
    
    set #start 0
    set #n_trgt_clr -1
    
    while #start = 0
        if 1003, 215 16777215 abs or #n_golden_frame != 0
            set #start 1
        end_if
        set #n_golden_frame findcolor (#bar_x1, #bar_y1 #bar_x2, #bar_y2 #golden_frame_color %golden_frame 2 abs)
    end_while
    
    wait 100
    move 1027, 482
    kleft 1027, 482
    wait 1s
    
    while #n_trgt_clr != 0
        set #n_trgt_clr findcolor (#bar_x1 #bar_y1 #bar_x2 #bar_y2 #cursor_color %arr 2 abs)
        set #n_target findimage (#bar_x1 #bar_y1 #bar_x2 #bar_y2 ("C:\Program Files\uopilot\img\target.png") %target_crds 2 70 1 5 abs)
        set #n_cursor findimage (#bar_x1 #bar_y1 #bar_x2 #bar_y2 ("C:\Program Files\uopilot\img\cursor.png") %cursor_crds 2 50 1 20 abs)
        if #n_target != 0 and #n_cursor != 0
            move 1027, 482
            set #cur_x %cursor_crds[1 1] + #cur_sdvig
            if #cur_x <= %target_crds[1 1]
                kleft_down 1027, 482
            else
                kleft_up 1027, 482
            end_if
        end_if
        if #n_target = 0 and #n_cursor != 0
            kleft_down 1027, 482
        end_if
    end_while
    kleft_up 1027, 482

Все файлы выложенные на сайте в основном сделаны мной для меня и не представляют никакой угрозы для кого-либо, но на всякий случай вынужден перед самим фалом добавить подобную фразу:

Все файлы скаченные с данного сайта вы используете на свой страх и риск. Владелец сайта не несет никакой ответственности за последствия. Запрещается использовать файлы в коммерческих целях, а также размещать на своих информационных ресурсах без указания первоисточника. Скачивая файлы, Вы соглашаетесь со всем перечисленным выше.

Архив с программой:

[https://disk.yandex.ru/d/bY2Ku\_oLBz33qw](https://disk.yandex.ru/d/bY2Ku_oLBz33qw)